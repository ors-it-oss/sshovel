#!/usr/bin/env python
'''
sshovel - encrypt files with ssh-agent, bury your secrets
'''

import io
import os
import os.path
import re
import signal
import subprocess
import sys
import tempfile
import unittest

class Main(object):
    @staticmethod
    def _test(test_args):
        unittest_args = [__file__, '--quiet']
        unittest_args.extend(test_args)
        unittest.main(argv=unittest_args)

class MainTest(unittest.TestCase):
    message = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

    def setUp(self):
        # ssh-agent
        self.ssh_fixture = SSHTestFixture()
        os.environ['SSH_AUTH_SOCK'] = self.ssh_fixture.socket_path
        # $EDITOR
        self.editor_tmp = None
        # Some test files
        self.tmps = [tempfile.mkstemp()[1] for _ in range(3)]
        with open(self.tmps[0], 'w') as writer:
            writer.write(self.message)

    def test_encrypt(self):
        tmp0, tmp1, tmp2 = self.tmps
        Main(['--cipher', 'openssl', tmp0, tmp1])
        self.assert_encrypted(tmp1)
        Main([tmp1, tmp2])
        self.assert_file_contents(tmp2, self.message)

    def test_edit(self):
        self.use_add_data_editor()
        tmp0, tmp1, _ = self.tmps
        # Edit in place
        Main(['--cipher', 'openssl', '--edit', tmp0])
        self.assert_encrypted(tmp0)
        # Decrypt
        Main([tmp0, tmp1])
        self.assert_file_contents(tmp1, self.message + 'DATA')

    def test_edit_plaintext_noop(self):
        self.use_noop_editor()
        tmp0 = self.tmps[0]
        time0 = 1234  # 1970-01-01 01:20:34 +0100
        os.utime(tmp0, (time0, time0))
        Main(['--cipher', 'openssl', '--edit', tmp0])
        time1 = os.stat(tmp0).st_mtime
        self.assert_encrypted(tmp0)
        self.assertNotEqual(time0, time1, 'mtime should have changed!')

    def test_edit_existing_noop(self):
        self.use_noop_editor()
        tmp0, tmp1, _ = self.tmps
        # Prepare an encrypted file
        Main(['--cipher', 'openssl', tmp0, tmp1])
        self.assert_encrypted(tmp1)
        # Edit, but don't actually change anything
        time0 = 1234  # 1970-01-01 01:20:34 +0100
        os.utime(tmp1, (time0, time0))
        Main(['--cipher', 'openssl', '--edit', tmp1])
        time1 = os.stat(tmp1).st_mtime
        self.assert_encrypted(tmp1)
        self.assertEqual(time0, time1, "mtime should be unchanged!")

    def test_edit_new_file(self):
        self.use_add_data_editor()
        tmp0, tmp1, _ = self.tmps
        os.remove(tmp0)
        Main(['--cipher', 'openssl', '--edit', tmp0])
        self.assert_encrypted(tmp0)
        Main([tmp0, tmp1])
        self.assert_file_contents(tmp1, "DATA")

    def assert_file_contents(self, path, expected):
        with open(path, 'r') as reader:
            self.assertEqual(reader.read(), expected)

    def assert_encrypted(self, path):
        self.assertTrue(ShovelHeader.is_shovel_file(path))

    def tearDown(self):
        self.ssh_fixture.stop()
        for path in self.tmps:
            os.unlink(path)
        if self.editor_tmp is not None:
            self.editor_tmp = None
            del os.environ['EDITOR']

    def use_add_data_editor(self):
        self.use_shell_script_editor("exec /bin/echo -n DATA >> $1")

    def use_noop_editor(self):
        self.use_shell_script_editor("exec touch $1")

    def use_shell_script_editor(self, script):
        tmp = tempfile.NamedTemporaryFile()
        tmp.write("#!/bin/sh\n")
        tmp.write(script)
        tmp.write("\n")
        tmp.flush()
        os.chmod(tmp.name, 0700)
        self.editor_tmp = tmp
        os.environ['EDITOR'] = tmp.name


class BinaryWriterTest(unittest.TestCase):
    def test_writer(self):
        age, name, sales = 58, 'Sade', 23500000
        msg = BinaryWriter().string(name).byte(age).long(sales).end()
        expected = (
            '\x00\x00\x00\x0d'
            '\x00\x00\x00\x04'
            '\x53\x61\x64\x65'
            '\x3a'
            '\x01\x66\x94\xe0')
        self.assertEqual(msg, expected)


class BinaryReaderTest(unittest.TestCase):
    def test_reader(self):
        buf = io.BytesIO(
            '\x00\x00\x00\x11'
            '\x00\x00\x00\x08'
            '\x4f\x6c\x64\x66\x69\x65\x6c\x64'
            '\x40'
            '\x00\x28\x21\x70')
        msg = BinaryReader(buf.read)
        self.assertEqual(msg.long(), 17)
        self.assertEqual(msg.string(), 'Oldfield')
        self.assertEqual(msg.byte(), 64)
        self.assertEqual(msg.long(), 2630000)


class SSHTestFixture(object):
    """
    Set up a real ssh-agent(1) and load it with a test key, and provide a way
    to kill the agent.
    """

    TEST_KEY = """
-----BEGIN RSA PRIVATE KEY-----
MIICXAIBAAKBgQD0OCPZ50akyXxhyFz/JdCTZISvHJ+nFOnXMHKQzF3Q3fAbXGVM
jEU2Wer+owj6s4wxuNmd6g3XAyyomCSRoxE6txNpQ10Yay4ZUMhO3XDr3zN5WBhd
6dqDjNLsrfu5mjy9aWFZDpBYnmRnOQBeLGxNQE6shbwOAzsixirmiUyXBQIDAQAB
AoGAZwCallgGIpBcZn10Q6S2UMQPdi/TYkveyITFfS7Ezsgccd3JV7y9oEvSYi1v
JxW9Jmd5WTITPkE3f7ATlF07cT5EZaHPMHm02GJegopN1AW2caoN2N+FHpe2cnOW
0vLtV+dQ0j5QnCWOfPpM70wYqEwvO+tC9uIaIOCBVTdyF20CQQD7IxTeZjiirZ0M
SlSPZmNRlFBRUkHjc4I7A5weV5L5YR/LWN2W/RL0j+v60L18Uld0pGcDuYX1Fg42
oUYEazaLAkEA+PLEMtU7vh3d9PtyDyIG7prcAJnUljwybRgZoVQNMsMxxONbSoAb
B9tw8irock7zjkPYsqvjSAxcfV1I+J6qrwJBAJaFQEzMF8XpKOfk5SnNxFlw+3LC
Spt47+VPFJNbCcxOWjAW4zlMFcBfQqDh27BX6fMPVm71E0UCIyK7JqwfVmECQAJ8
8qcLaIhy5ff/11j9XxJda9t5rh0+Rsa+Wes52tPqDYJJP21UMHD4qX1SHnaeAWMn
nG/UtfXPYdFC8GrDszMCQHNHjkRPhgnFKZBIqg6CjWE0wVWdZRWwLrP7a2YQsX4A
aZkvLueqxAr5SzU9sTiL6tBQAEaESEHOTm11g+IRmFA=
-----END RSA PRIVATE KEY-----
"""

    def __init__(self):
        self.start_agent()
        self.load_keys()

    def start_agent(self):
        output = subprocess.check_output('ssh-agent')
        self.socket_path = re.search('SSH_AUTH_SOCK=(.+?);', output).group(1)
        pid_str = re.search(r'SSH_AGENT_PID=(\d+)', output).group(1)
        self.agent_pid = int(pid_str)

    def load_keys(self):
        key_name = 'a_test_ssh_key'
        key_file = tempfile.NamedTemporaryFile(
            dir='/tmp',
            prefix=key_name + '.')
        key_file.write(self.TEST_KEY)
        key_file.flush()
        self._ssh_add(key_file.name)

    def delete_keys(self):
        self._ssh_add('-D')

    def _ssh_add(self, *args):
        command = ['ssh-add']
        command.extend(args)
        env = dict(os.environ)
        env['SSH_AUTH_SOCK'] = self.socket_path
        subprocess.check_call(command, env=env, stderr=subprocess.PIPE)

    def stop(self):
        os.kill(self.agent_pid, signal.SIGKILL)


class SSHAgentTest(unittest.TestCase):
    def setUp(self):
        self.ssh_fixture = SSHTestFixture()

    def test_identities(self):
        ssh_agent = SSHAgent(self.ssh_fixture.socket_path)
        keys = ssh_agent.identities()
        self.assertEqual(len(keys), 1, "number of keys loaded in agent")

    def test_sign(self):
        ssh_agent = SSHAgent(self.ssh_fixture.socket_path)
        key = ssh_agent.identities()[0]
        message = "Hello, world!"
        expected = (
            '\x23\xe4\x80\x83\xfb\x29\xb3\xf2\x02\x6a\xce\xe8\x56\x65\x51\xf6'
            '\x03\xe8\x45\x4f\x49\x3b\x32\x27\x7c\xcc\xaf\xa7\xbd\x27\x52\x8b'
            '\x76\xc5\x30\xcf\xb0\x79\x55\xeb\x9a\xc7\xbd\x6b\x42\x20\xf1\xbc'
            '\xae\x4c\x29\xfa\x8b\x2e\x9d\x94\x7c\x1f\xb8\x33\x1f\xd9\x89\x9f'
            '\xd9\xac\x62\xce\x6b\x83\x9c\xc5\xbd\xe0\x95\xca\xc8\x76\xa0\xdd'
            '\x94\xc5\x34\x73\xcf\x5e\x4d\xcb\x4d\xfc\x8d\x85\x72\xe2\x33\xc8'
            '\xff\x82\xeb\xb3\x6a\xa8\xf4\xc6\x55\x6e\x76\x64\xd9\xfe\xc2\x67'
            '\x39\x67\x92\xab\xd1\x2d\x74\x89\x13\x4a\x65\xa1\x46\xa8\x6a\xfa')
        flags = SSHAgent.SIGN_FLAG_NONE
        for _ in range(0, 3):
            signature = ssh_agent.sign(key.blob, message, flags)
            self.assertEqual(signature, expected)

    def tearDown(self):
        self.ssh_fixture.stop()


class CipherTest(unittest.TestCase):
    message = 'Hello, world!'

    def assert_encrypt(self, cipher, passphrase):
        reader = io.BytesIO(self.message)
        writer = io.BytesIO()
        cipher.encrypt(reader, writer, passphrase)
        return writer.getvalue()

    def assert_decrypt(self, cipher, ciphertext, passphrase):
        reader = io.BytesIO(ciphertext)
        writer = io.BytesIO()
        cipher.decrypt(reader, writer, passphrase)
        self.assertEqual(
            self.message,
            writer.getvalue(),
            'decrypting with %s' % cipher.__class__.__name__)


class OpenSSLCipherTest(CipherTest):
    def test_cipher(self):
        cipher = OpenSSLCipher()
        passphrase = 'PASSPHRASE'
        ciphertext = self.assert_encrypt(cipher, passphrase)
        self.assert_decrypt(cipher, ciphertext, passphrase)


class ScryptCipherTest(CipherTest):
    def test_cipher(self):
        cipher = ScryptCipher(encrypt_options=['-t', '0.1'])
        passphrase = 'PASSPHRASE'
        ciphertext = self.assert_encrypt(cipher, passphrase)
        self.assert_decrypt(cipher, ciphertext, passphrase)


class ShovelTest(CipherTest):
    def setUp(self):
        self.fixture = SSHTestFixture()

    def tearDown(self):
        self.fixture.stop()

    def test_cipher(self):
        ssh_agent = SSHAgent(self.fixture.socket_path)
        key = ssh_agent.identities()[0]
        message = "Hello, world!!!!1"
        cipher = OpenSSLCipher()

        # Encrypt
        src = io.BytesIO(message)
        dst = io.BytesIO()
        ShovelParameters.random(ssh_agent, key, cipher).encrypt(src, dst)

        # Decrypt
        dst.seek(0)
        src = dst
        dst = io.BytesIO()
        ShovelParameters.read_from(src, ssh_agent).decrypt(src, dst)

        plaintext = dst.getvalue()
        self.assertEqual(plaintext, message)

    def test_missing_keys(self):
        ssh_agent = SSHAgent(self.fixture.socket_path)
        key = ssh_agent.identities()[0]
        cipher = OpenSSLCipher()
        params = ShovelParameters.random(ssh_agent, key, cipher)
        dst = params.encrypt(io.BytesIO(), io.BytesIO())
        self.fixture.delete_keys()
        ex = None
        try:
            params.decrypt(dst, io.BytesIO())
        except UserException as ex:
            pass
        self.assertIsNotNone(ex)
        self.assertTrue("missing key" in str(ex))


class LintTest(unittest.TestCase):
    def test_pep8(self):
        self._lint_with('pep8')

    def test_pylint(self):
        self._lint_with('pylint', '-f', 'parseable')

    @staticmethod
    def _lint_with(*command):
        command = list(command)
        command.append(__file__)
        try:
            subprocess.check_call(command)
        except OSError:
            pass


Main(sys.argv[1:])

#!/usr/bin/env python
'''
sshovel - encrypt files with ssh-agent, bury your secrets
'''

# Python libs
import argparse
import base64
import fcntl
import hashlib
import io
import os
import os.path
import pty
import re
import shutil
import signal
import socket
import struct
import subprocess
import sys
import tempfile
import time

# Logging & Debugging
import logging
import pprint

# 3rd party

# Own


log = logging.getLogger()
logging.basicConfig(
    level=logging.getLevelName(os.environ.get('LOG_LEVEL', 'info').upper()),
    format='%(levelname)s %(message)s',
    handlers=[logging.StreamHandler(sys.stderr)]
)

ppr = pprint.PrettyPrinter(indent=2).pprint


class UserException(Exception):
    """
    Exception for alerting the user with messages (and not full stack traces.)
    """
    pass


def log(message):
    script = os.path.basename(sys.argv[0])
    message = "{}: {}\n".format(script, message)
    sys.stderr.write(message)


class BinaryReader(object):
    """
    Wrapper around `struct` for reading SSH binary messages.
    """

    @classmethod
    def from_string(cls, string):
        return cls(io.BytesIO(string).read)

    def __init__(self, read_fn):
        self.read_fn = read_fn

    def byte(self):
        return self._read(1, '>B')

    def long(self):
        return self._read(4, '>L')

    def string(self):
        length = self.long()
        return self._read(length, '>%ds' % length)

    def _read(self, length, template):
        return struct.unpack(template, self.read_fn(length))[0]


class BinaryWriter(object):
    """
    Wrapper around `struct` for building a buffer of SSH binary messages.
    """

    def __init__(self):
        self.buf = io.BytesIO()

    def byte(self, value):
        return self._pack('>B', value)

    def long(self, value):
        return self._pack('>L', value)

    def string(self, value):
        string = str(value)
        length = len(string)
        return self._pack('>L%ds' % length, length, string)

    def end(self):
        return str(BinaryWriter().string(self))

    def __str__(self):
        return self.buf.getvalue()

    def _pack(self, template, *values):
        self.buf.write(struct.pack(template, *values))
        return self


class SSHKey(object):
    ALGORITHM_RSA = 'ssh-rsa'

    @classmethod
    def read(cls, reader, fingerprinter):
        blob = reader.string()
        comment = reader.string()
        algorithm = BinaryReader.from_string(blob).string()
        fingerprint = fingerprinter(blob)
        return cls(blob, comment, algorithm, fingerprint)

    def __init__(self, blob, comment, algorithm, fingerprint):
        self.blob = blob
        self.comment = comment
        self.algorithm = algorithm
        self.fingerprint = fingerprint

    def __str__(self):
        return '%s "%s"' % (self.fingerprint, self.comment)

    def is_rsa(self):
        return self.algorithm == self.ALGORITHM_RSA

    @staticmethod
    def fingerprint_md5(blob):
        digest = hashlib.md5(blob).hexdigest()
        pairs = re.findall('(..)', digest)
        return 'MD5:' + ':'.join(pairs)

    @staticmethod
    def fingerprint_sha256(blob):
        digest = hashlib.sha256(blob).digest()
        return 'SHA256:' + base64.b64encode(digest).rstrip('=')


class SSHAgent(object):
    """
    Interface to a running ssh-agent(1)
    """

    # https://tools.ietf.org/id/draft-miller-ssh-agent-01.html
    AGENTC_REQUEST_IDENTITIES = 11
    AGENT_IDENTITIES_ANSWER = 12
    AGENTC_SIGN_REQUEST = 13
    AGENT_SIGN_RESPONSE = 14
    AGENT_FAILURE = 5

    SIGN_FLAG_NONE = 0
    SIGN_FLAG_RSA_SHA2_256 = 2
    SIGN_FLAG_RSA_SHA2_512 = 4

    def __init__(self, socket_path, fingerprinter=SSHKey.fingerprint_sha256):
        self.sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM, 0)
        self.fingerprinter = fingerprinter
        try:
            self.sock.connect(socket_path)
        except IOError as ex:
            raise UserException(
                "can't connect to ssh-agent socket %s: %s" % (socket_path, ex))

    def identities(self):
        self.sock.sendall(
            BinaryWriter()
            .byte(SSHAgent.AGENTC_REQUEST_IDENTITIES)
            .end())
        reader = BinaryReader(self.sock.recv)
        _ = reader.long()  # length
        response = reader.byte()
        assert response == SSHAgent.AGENT_IDENTITIES_ANSWER
        num_keys = reader.long()
        result = []
        for _ in range(num_keys):
            key = SSHKey.read(reader, self.fingerprinter)
            result.append(key)
        return result

    def sign(self, key_blob, message, flags):
        self.sock.sendall(
            BinaryWriter()
            .byte(SSHAgent.AGENTC_SIGN_REQUEST)
            .string(key_blob)
            .string(message)
            .long(flags)
            .end())
        reader = BinaryReader(self.sock.recv)
        _ = reader.long()  # length
        response = reader.byte()
        if response == SSHAgent.AGENT_FAILURE:
            return None
        assert response == SSHAgent.AGENT_SIGN_RESPONSE
        _ = reader.long()    # signature length
        _ = reader.string()  # signature format: ssh-dsa, ssh-rsa, ssh-ed25519
        signature = reader.string()
        return signature


class Cipher(object):
    """
    Interface / superclass for command line encryption tools.
    Plus some tools for finding the subclasses of Cipher.
    """

    def __init__(self, **options):
        self.options = options

    def encrypt(self, reader, writer, passphrase):
        pass

    def decrypt(self, reader, writer, passphrase):
        pass

    def name(self):
        return Cipher.name_of(self.__class__)

    def __str__(self):
        return self.name()

    @staticmethod
    def name_of(a_cls):
        return a_cls.__name__.lower().replace('cipher', '')

    @staticmethod
    def all_cipher_names():
        return [
            Cipher.name_of(cls)
            for cls in Cipher.__subclasses__()]

    @staticmethod
    def instance_of(name):
        for cls in Cipher.__subclasses__():
            if Cipher.name_of(cls) == name.lower():
                return cls()
        raise UserException("unknown cipher '{}'".format(name))


class OpenSSLCipher(Cipher):
    """
    Using OpenSSL as a command line encryption tool is weaker than scrypt.  The
    passphrase is hashed a few times but it's nothing like as complex as
    scrypt's key derivation function.

    However, the signatures ssh-agent are at least 128 bytes long, which is
    roughly a 150 character password.
    """

    def encrypt(self, reader, writer, passphrase):
        self._openssl('-e', reader, writer, passphrase)

    def decrypt(self, reader, writer, passphrase):
        self._openssl('-d', reader, writer, passphrase)

    @staticmethod
    def _openssl(command, reader, writer, passphrase):
        password_read, password_write = os.pipe()
        os.write(password_write, passphrase)
        os.close(password_write)
        command = [
            'openssl', 'aes-256-cbc',
            command,
            '-a', '-salt',
            '-kfile', '/dev/fd/%d' % password_read]
        try:
            openssl = subprocess.Popen(
                command,
                stdin=subprocess.PIPE,
                stdout=subprocess.PIPE)
            shutil.copyfileobj(reader, openssl.stdin)
            openssl.stdin.close()
            shutil.copyfileobj(openssl.stdout, writer)
            openssl.wait()
        finally:
            os.close(password_read)


class Expect(object):
    def __init__(self, command):
        self.command = command
        self._start()

    def expect(self, phrase, timeout):
        buf = ''
        for block in self._read(self.tty_read, timeout):
            buf += block
            if phrase in buf:
                return True
        raise Exception("EOF")

    def tty_read(self, size):
        return os.read(self.tty, size)

    def send(self, data):
        os.write(self.tty, data)

    def copy(self, reader, writer, timeout):
        for block in self._read(reader.read, timeout):
            writer.write(block)

    def finish(self):
        os.close(self.tty)
        self.stdout.close()
        return os.waitpid(self.pid, 0)

    def _start(self):
        stdin_reader, stdin_writer = os.pipe()
        stdout_reader, stdout_writer = os.pipe()

        pid, tty = pty.fork()
        if not pid:
            os.close(stdin_writer)
            os.close(stdout_reader)
            os.dup2(stdin_reader, 0)
            os.dup2(stdout_writer, 1)
            os.execvp(self.command[0], self.command)

        self._set_non_blocking(tty)
        self._set_non_blocking(stdout_reader)

        os.close(stdin_reader)
        os.close(stdout_writer)

        self.tty = tty
        self.pid = pid
        self.stdin = os.fdopen(stdin_writer, 'w')
        self.stdout = os.fdopen(stdout_reader, 'r')

    @staticmethod
    def _set_non_blocking(file_descriptor):
        fcntl.fcntl(file_descriptor, fcntl.F_SETFL, os.O_NONBLOCK)

    @staticmethod
    def _read(read_fn, timeout):
        start_time = time.time()
        while True:
            duration = time.time() - start_time
            if duration > timeout:
                raise Exception("timed out")
            block = None
            try:
                block = read_fn(1024)
                if block == '':
                    break
            except (IOError, OSError):
                time.sleep(0.1)
                continue
            yield block


class ScryptCipher(Cipher):
    """
    Interface to the scrypt(1) command line tool.
    """

    def encrypt(self, reader, writer, passphrase):
        command = ['scrypt', 'enc']
        command.extend(self.options.get('encrypt_options', []))
        command.append('-')
        self.scrypt(command, reader, writer, passphrase, passphrase)

    def decrypt(self, reader, writer, passphrase):
        command = ['scrypt', 'dec', '-']
        self.scrypt(command, reader, writer, passphrase)

    @staticmethod
    def scrypt(command, reader, writer, *passphrases):
        log('passing data to scrypt')
        exp = Expect(command)
        for passphrase in passphrases:
            exp.expect('passphrase: ', 1)
            exp.send(passphrase + "\n")
        exp.copy(reader, exp.stdin, 10)
        exp.stdin.close()
        exp.copy(exp.stdout, writer, 10)
        exp.finish()


class ShovelParameters(object):
    """
    ShovelParameters wraps everything required to encrypt or decrypt a
    sshovel file:

        - connection to an agent
        - an ssh-key
        - a nonce used to generate a password
        - an underlying instance of Cipher

    You can either make a new one with random data, or parse a stream
    containing an existing sshovel file.
    """

    NONCE_SIZE = 1024

    @classmethod
    def random(cls, ssh_agent, key, cipher):
        nonce = os.urandom(cls.NONCE_SIZE)
        return cls(ssh_agent, key, cipher, nonce)

    @classmethod
    def read_from(cls, reader, ssh_agent):
        header = ShovelHeader.read_from(reader)
        key = cls._find_key(ssh_agent, header.nonce, header.key_hash)
        cipher = Cipher.instance_of(header.cipher_name)
        return cls(ssh_agent, key, cipher, header.nonce)

    def __init__(self, ssh_agent, key, cipher, nonce):
        self.ssh_agent = ssh_agent
        self.key = key
        self.cipher = cipher
        self.nonce = nonce

    def encrypt(self, reader, writer):
        writer.write(str(self._header()))
        log('encrypting with %s and %s' % (self.key, self.cipher))
        self.cipher.encrypt(reader, writer, self._passphrase())
        return writer

    def decrypt(self, reader, writer):
        log('decrypting with %s and %s' % (self.key, self.cipher))
        self.cipher.decrypt(reader, writer, self._passphrase())
        return writer

    def _header(self):
        return ShovelHeader(
            self.cipher.name(),
            self.nonce,
            self._create_key_hash(self.nonce, self.key))

    def _passphrase(self):
        flags = SSHAgent.SIGN_FLAG_NONE
        if self.key.is_rsa():
            flags = SSHAgent.SIGN_FLAG_RSA_SHA2_512
        signature = self.ssh_agent.sign(self.key.blob, self.nonce, flags)
        if signature is None:
            raise UserException("agent is missing key '%s'" % self.key.comment)
        return hashlib.sha1(signature).hexdigest()

    @classmethod
    def _find_key(cls, ssh_agent, nonce, key_hash):
        for key in ssh_agent.identities():
            if cls._create_key_hash(nonce, key) == key_hash:
                return key
        raise UserException("agent doesn't have the required key")

    @staticmethod
    def _create_key_hash(nonce, key):
        return hashlib.sha1(nonce + key.blob).digest()


class ShovelHeader(object):
    """
    Serialize and deserialize sshovel parameters from a stream.
    """

    MAGIC = "HAZ.CAT/SSHOVEL"
    VERSION = 5807

    @classmethod
    def is_shovel_file(cls, path):
        with io.open(path, mode='rb', buffering=1024) as reader:
            return cls.is_shovel_stream(reader)

    @classmethod
    def is_shovel_stream(cls, reader):
        length = len(cls.MAGIC)
        block = reader.peek(length)
        actual = block[0:length]
        return actual == cls.MAGIC

    @classmethod
    def read_from(cls, reader):
        magic = reader.read(len(cls.MAGIC))
        assert magic == cls.MAGIC
        header = BinaryReader(reader.read)
        version = header.long()
        assert version == cls.VERSION
        cipher_name = header.string()
        nonce = header.string()
        num_key_hashes = header.long()
        assert num_key_hashes == 1
        key_hash = header.string()
        return cls(cipher_name, nonce, key_hash)

    def __init__(self, cipher_name, nonce, key_hash):
        self.cipher_name = cipher_name
        self.nonce = nonce
        self.key_hash = key_hash

    def __str__(self):
        header = BinaryWriter()
        header.long(self.VERSION)
        header.string(self.cipher_name)
        header.string(self.nonce)
        header.long(1)
        header.string(self.key_hash)
        return '%s%s' % (self.MAGIC, header)


class Editor(object):
    DEFAULT = 'nano'

    @classmethod
    def default(cls):
        editor = os.environ.get('EDITOR', cls.DEFAULT)
        return cls(editor)

    def __init__(self, binary):
        self.binary = binary

    def edit(self, path):
        cksum_before = self._cksum(path)
        command = [self.binary, path]
        pid = os.fork()
        if pid == 0:
            os.execvp(command[0], command)
        _, status = os.waitpid(pid, 0)
        status = status >> 8
        if status != 0:
            raise UserException(
                "error; editor %s exited with status %d" %
                (self.binary, status))
        cksum_after = self._cksum(path)
        return cksum_after != cksum_before

    @staticmethod
    def _cksum(path):
        buf = hashlib.sha1()
        if os.path.exists(path):
            with io.open(path, mode='rb') as reader:
                block = reader.read(1 << 16)
                buf.update(block)
        return buf.hexdigest()


class Main(object):
    # This class keeps methods in one place; it's not really a class!
    # pylint: disable=too-few-public-methods

    def __init__(self, args):
        signal.signal(signal.SIGQUIT, self._quit)
        args = self._parse_args(args)
        try:
            self._main(args)
        except UserException as ex:
            log(ex.message)
            sys.exit(1)
        except KeyboardInterrupt:
            log('interrupted; exiting')
            sys.exit(1)

    def _main(self, args):
        if args.test is not None:
            self._test(args.test)

        ssh_agent = self._find_agent(args.fingerprinter_name)
        cipher = Cipher.instance_of(args.cipher)
        keys = ssh_agent.identities()
        if not keys:
            raise UserException("ssh agent has no keys")

        key = keys[0]
        if args.match_key_comment:
            key = self._match_key(keys, args.match_key_comment)

        if args.edit:
            path = args.edit
            return self._edit(path, ssh_agent, key, cipher)

        in_file = args.in_file
        if in_file == '-':
            log("reading from stdin")
            in_file = '/dev/stdin'

        out_file = args.out_file
        if out_file == '-':
            out_file = '/dev/stdout'

        # io.open's buffering gives us peek()
        try:
            reader = io.open(in_file, mode='rb', buffering=1024)
            writer = open(out_file, mode='wb')
        except IOError as ex:
            raise UserException(ex)

        if ShovelHeader.is_shovel_stream(reader):
            (ShovelParameters
             .read_from(reader, ssh_agent)
             .decrypt(reader, writer))
        else:
            (ShovelParameters
             .random(ssh_agent, key, cipher)
             .encrypt(reader, writer))

    @staticmethod
    def _quit(*args):
        # pylint: disable=unused-argument
        raise KeyboardInterrupt()

    @staticmethod
    def _edit(path, ssh_agent, key, cipher):
        tmp_dir = tempfile.mkdtemp()
        tmp = os.path.join(tmp_dir, os.path.basename(path))
        is_new = not os.path.exists(path)
        is_plaintext = not (is_new or ShovelHeader.is_shovel_file(path))
        try:
            params = None
            if is_new:
                log('new file, no decryption needed!')
                time.sleep(0.700)
            else:
                if is_plaintext:
                    shutil.copyfile(path, tmp)
                else:
                    with open(path, 'rb') as reader, open(tmp, 'wb') as writer:
                        params = ShovelParameters.read_from(reader, ssh_agent)
                        params.decrypt(reader, writer)

            if params is None:
                params = ShovelParameters.random(ssh_agent, key, cipher)

            editor = Editor.default()
            log('editing %s with %s' % (tmp, editor.binary))
            is_changed = editor.edit(tmp)
            if is_new or is_plaintext or is_changed:
                if is_new:
                    log('new file, encrypting')
                if is_plaintext:
                    log('original file was plaintext, encrypting')
                if is_changed:
                    log('edits were madge, re-encrypting')
                with open(tmp, 'rb') as reader, open(path, 'wb') as writer:
                    params.encrypt(reader, writer)
            else:
                log('no edits, skipping encrypt')
        finally:
            if os.path.exists(tmp):
                os.remove(tmp)
            shutil.rmtree(tmp_dir)

    @staticmethod
    def _parse_args(args):
        usage = '''\
%(prog)s [--cipher CIPHER] [IN] [OUT]
       %(prog)s --edit FILE
'''

        epilogue = u"""\
examples:

  - Default is to use scrypt(1) to encrypt:

      $ sshovel message.txt message.txt.enc


  - If the input is encrypted, then the default action is to decrypt:

      $ sshovel message.txt.enc
      Hello, world!


  - Edit an encrypted file in place (or edit then encrypt, if it's plaintext):

      $ sshovel --edit secrets


  - Use a specific agent key (only needed for encrypt):

      $ sshovel --key my_other_key
\t
"""  # <- The tab is needed to force a blank line at the end of the epilogue.

        parser = argparse.ArgumentParser(
            usage=usage,
            formatter_class=argparse.RawDescriptionHelpFormatter,
            description='Encrypt files with ssh-agent, bury your secrets',
            epilog=epilogue)
        parser.add_argument(
            '--key',
            metavar='MATCH',
            dest='match_key_comment',
            help='use the ssh key whose comment matches MATCH')
        parser.add_argument(
            '--cipher',
            dest='cipher',
            default=os.environ.get('SSHOVEL_CIPHER', 'scrypt'),
            metavar='TOOL',
            choices=Cipher.all_cipher_names(),
            help=(
                'encrypt with "scrypt" or "openssl" '
                '(this can also be set in the environment '
                'with SSHOVEL_CIPHER)'))
        parser.add_argument(
            '--fingerprint-hash',
            dest='fingerprinter_name',
            default='sha256',
            metavar='HASH',
            choices=('md5', 'sha256'),
            help='algorithm used to display fingerprints')
        parser.add_argument(
            '--edit',
            metavar='FILE',
            help='decrypt, edit with $EDITOR, and re-encrypt')
        parser.add_argument(
            '--test',
            nargs='*',
            metavar='ARGS',
            help='run the test suite')
        parser.add_argument(
            'in_file',
            nargs='?',
            default='-',
            metavar='IN',
            help='optional path, or "-" for stdin, which is the default')
        parser.add_argument(
            'out_file',
            nargs='?',
            default='-',
            metavar='OUT',
            help='as above, with stdout as the default')
        return parser.parse_args(args)

    @staticmethod
    def _find_agent(fingerprinter_name):
        fingerprinter = None
        if fingerprinter_name == 'md5':
            fingerprinter = SSHKey.fingerprint_md5
        elif fingerprinter_name == 'sha256':
            fingerprinter = SSHKey.fingerprint_sha256
        else:
            raise UserException(
                'unknown fingerprint hash "%s"' % fingerprinter_name)
        socket_path = os.environ.get('SSH_AUTH_SOCK')
        if socket_path == "":
            raise UserException("SSH_AUTH_SOCK is empty or unset")
        return SSHAgent(socket_path, fingerprinter)

    @staticmethod
    def _match_key(keys, pattern):
        matches = []
        for candidate in keys:
            if pattern in candidate.comment:
                matches.append(candidate)
        if len(matches) == 1:
            return matches[0]
        elif not matches:
            raise UserException(
                "no ssh key matched '{}'; known keys: {}"
                .format(pattern, [k.comment for k in keys]))
        elif len(matches) > 2:
            raise UserException(
                "more than one key matched '{}': {}"
                .format(pattern, [k.comment for k in matches]))


Main(sys.argv[1:])
